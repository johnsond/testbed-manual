#lang scribble/manual

@(require "defs.rkt")

@title[#:tag "powder-profiles" #:version apt-version]{@(tb) starter profiles}

@itemlist[

@item{The @hyperlink["https://www.powderwireless.net/p/PowderProfiles/ota_srslte"]{ota_srslte profile} enables the instantiation of an end-to-end @hyperlink["https://github.com/srsLTE/srsLTE"]{srsLTE} configuration consisting of an SDR-based UE, an SDR-based eNodeB and an EPC core network. (See the chapter on @seclink["ota"]{over-the-air operation} for a step-by-step example using this profile.)}

@item{The @hyperlink["https://www.powderwireless.net/p/PowderProfiles/ota_monitor"]{ota_monitor profile} enables basic spectrum monitoring functionality using an SDR and the @hyperlink["https://files.ettus.com/manual/"]{USRP Hardware Driver (UHD)} software tools.}

@;{@item{The @hyperlink["https://www.powderwireless.net/p/PowderTeam/ota_gnuradio"]{ota_gnuradio profile} provides an example of using @hyperlink["https://www.gnuradio.org"]{GNU Radio} software, combined with SDRs, to perform the transmission/reception of a simple file using QPSK over OFDM.}
}

@item{The @hyperlink["https://www.powderwireless.net/p/renew/renewlab"]{renewlab profile} enables the instantiation of an experiment using the @(tb) massive MIMO equipment. The @hyperlink["https://renew-wireless.org"]{RENEW} website provides more information on the massive MIMO equipment. Including a @hyperlink["https://obejarano.gitlab.io/renew-documentation/getting-started/powder/"]{walkthrough} on instantiating the profile and @hyperlink["https://obejarano.gitlab.io/renew-documentation/dev-suite/design-flows/python-design-flow/"]{tutorials} and other information on using the equipment.}

@item{The @hyperlink["https://www.powderwireless.net/p/PowderProfiles/oai_nr_sim"]{oai_nr_sim profile} instantiates two compute nodes and provides step-by-step instructions for compiling and running the OpenAirInterface 5G NR code base in a simulated setup.}

@item{The 
@hyperlink["https://www.powderwireless.net/p/PowderProfiles/oia_nr_sdr_lab"]{oai_nr_sdr_lab profile} enables the instantiation of the OpenAirInterface 5G NR code base for over-the-air operation in a lab environment. (Note that the code based used in this profile is fairly dated.)
}

]
