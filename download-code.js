// Hacky funcion that decodes all HTML character entities, but leaves the rest of
// the string intact.
function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}

function markupOneExampleForDownload(elem) {
    var link = document.createElement('a');
    link.setAttribute('download', "example.py");
    link.setAttribute('class', "code-sample-download-link");
    link.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(decodeHtml(elem.innerHTML)));
    link.innerHTML = "Download this example";
    elem.parentNode.insertBefore(link, elem.nextSibling);
}

function markupAllExamples() {
    var samples = document.getElementsByClassName("downloadable-code-sample");
    for(var i = 0; i < samples.length; i++) {
        markupOneExampleForDownload(samples.item(i));
    }
}

addEventListener('DOMContentLoaded', markupAllExamples, false);
