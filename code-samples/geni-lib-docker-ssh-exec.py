"""An example of a Docker container running an external, unmodified image, and customizing its remote access."""

import geni.portal as portal
import geni.rspec.pg as rspec

request = portal.context.makeRequestRSpec()
node = request.DockerContainer("node")
node.docker_extimage = "ubuntu:16.04"
node.docker_ssh_style = "exec"
node.docker_exec_shell = "/bin/bash"
portal.context.printRequestRSpec()
