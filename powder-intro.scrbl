#lang scribble/manual

@(require "defs.rkt")

@title[#:tag "powder-overview"]{@(tb) Overview}

@section[#:tag "whatis"]{What is @(tb)}

@(tb) is a remotely accessible "living laboratory" to enable mobile and wireless research. @(tb) is deployed in a "real world" environment/configuration with radio equipment, fiber infrastructure, edge-compute and datacenter/cloud resources deployed in a "city-scale" platform. 

In concert with broad industry trends towards network "softwarization", and to provide the greatest flexibility in terms of the research that can be enabled, @(tb) is an end-to-end software-defined-infrastructure. Specifically, @(tb) uses off-the-shelf hardware (software-defined-radios (SDRs), general purpose compute, state-of-the-art networking etc), and pairs that with a variety of software stacks to enable specific functionality of interest to platform users.

For example:
@itemlist[

@item{The @(tb) SDR infrastructure, combined with low level  @hyperlink["https://files.ettus.com/manual/"]{SDR specific} and @hyperlink["https://www.gnuradio.org"]{generic radio software} form the basis for wireless communication research (e.g., dynamic/shared spectrum use, novel radio frequency waveforms to improve spectral efficiency, spectrum monitoring, signal classification etc.)}

@item{The @(tb) SDR, edge-compute and datacenter/cloud infrastructure, combined with @hyperlink["https://www.srslte.com"]{4G} and @hyperlink["https://www.openairinterface.org"]{5G} software stacks enable end-to-end mobile networking research (e.g., radio access network (RAN) and core network (CN) protocol analysis, enhancement and evolution, cross-layer approaches to enhance performance, tradeoffs related to different RAN fronthaul splits, mobile network management and operations research etc.)}

@item{The @(tb) SDR, edge-compute and datacenter/cloud infrastructure, combined with @hyperlink["https://www.opennetworking.org"]{network service platform}, @hyperlink["https://www.onap.org"]{policy driven orchestration}, @hyperlink["https://www.lfedge.org"]{edge compute}, or @hyperlink["https://www.o-ran.org"]{RAN ecosystem} software stacks enable edge compute, RAN virtualization, network-function-virtualization (NFV) and orchestration research, (e.g., scalability/performance/management of virtualized network platforms, use cases/applications/service abstractions enabled by network softwarization,  edge compute abstractions, uses cases and scalability etc.)}

]

In addition to the above end-to-end software-defined infrastructure, @(tb) also has purpose built infrastructure to enable massive MIMO (mMIMO) related research. Specifically, @(tb) contains mMIMO base-stations and endpoints from @hyperlink["https://www.skylarkwireless.com"]{Skylark Wirelss} and mMIMO specific software from our @hyperlink["https://renew-wireless.org"]{RENEW} partners. The equipment enables a broad range of mMIMO research, including beamforming, scheduling, interference management etc. More details on the mMIMO equipment and software is available on the @hyperlink["https://renew-wireless.org"]{RENEW} website.

Note that @(tb) provides @seclink["powder-profiles"]{example profiles} for many of the scenarios described above. (@seclink["creating-profiles"]{@(tb) profiles} are "recipes" that describe the hardware and software needed to enable specific scenarios and can be programmatically "instantiated" into @seclink["experiments"]{experiments} that can be used as a starting point for research.)

Finally, @(tb) also enables "bring-your-own-device" related research. Specifically, the @(tb) platform is designed and built to allow third-party equipment to be deployed anywhere in the overall @(tb) architecture, and be integrated with the rest of the platform and its experimental workflow.  

@section[#:tag "powderstatus"]{@(tb) status}

@(tb) is deployed and operational on the campus of the University of Utah. A map of our current deployment is @hyperlink["https://www.powderwireless.net/map"]{here}. (A schedule outlining our further deployment plans is available from the @hyperlink["https://www.powderwireless.net"]{@(tb) portal}.)

More detailed information on the equipment and configuration of the current deployment is described in the @seclink["hardware"]{hardware} chapter.

Using @(tb) is free for NSF funded academics. Short term use of the platform by other users (non-NSF funded academic use, industry or government use etc.), for the purpose of evaluating the platform, is also free. Use of the platform beyond such short-term evaluation will require payment. We expect to have rates, payment mechanisms etc., in place in the near future.

@section[#:tag "roadmap"]{Roadmap to using @(tb)}

The @(tb) platform is a sophisticated instrument and it is often confusing for new users to understand what it is and how it can be used. Below is a suggested "roadmap" for exploring @(tb).

@itemlist[

@item{If you are new to @(tb), the @seclink["getting-started"]{getting started} exercise is a good place to start. This activity will walk you through the basic @(tb) experimental workflow (@seclink["register"]{creating a user account and joining an existing project}, @seclink["experiments"]{instantiating an experiment}, interacting with your instantiated experiment, terminating your experiment etc.) You should only need a Web browser (Chrome seems to work well) for this activity.}

@item{Access to radio resources in @(tb) is enabled on a per @seclink["projects"]{project} basis. Also, over-the-air (OTA) operation in @(tb) requires radio resources to be @seclink["reservations"]{reserved} ahead of time. The @seclink["ota"]{over-the-air operation} exercise provides a step-by-step walkthrough of the @(tb) over-the-air experimental workflow.}

@item{Using a browser to interact with the resources in your @seclink["experiments"]{experiment} is convenient for trying things out, but for most @(tb) users @seclink["ssh-access"]{setting up ssh access to the platform} and @seclink["x11"]{enabling X11} will be necessary.}

@item{The @seclink["powder-tutorial-srs"]{basic srsLTE tutorial} chapter provides another step-by-step activity for instantiating srsLTE in a simulated configuration.}

@item{The @seclink["openstack-tutorial"]{OpenStack tutorial} chapter provides a step-by-step exercise to instantiate an OpenStack cloud platform.}

@item{Once you have completed your account setup and the above step-by-step workflow activities, you might want to explore @(tb) concepts and mechanisms in more detail (i.e., @seclink["basic-concepts"]{basic platform concepts}, @seclink["creating-profiles"]{the profile mechanism}, @seclink["reservations"]{resource reservations}, @seclink["geni-lib"]{using python and geni-lib} in your profiles, etc. In order to use @(tb) it will also be useful to understand details about the platform @seclink["hardware"]{hardware and configuration}.}
 
@item{The chapter on @seclink["powder-profiles"]{@(tb) profiles} provides a description of a number of different "starter" profiles that might be useful for bootstrapping your use of the platform.}  
 
]


