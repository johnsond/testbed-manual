FROM ubuntu:latest
MAINTAINER Robert Ricci "ricci@cs.utah.edu"

RUN apt-get update
RUN apt-get install -y racket python python-pip mercurial make git
RUN pip install --upgrade sphinx
RUN pip install sphinx_rtd_theme
RUN pip install lxml